TEMPLATE   = subdirs
SUBDIRS   += Core\
             PEG\
             Tests\
             PropertyWidget\

PEG.depends = Core
Tests.depends = Core
PropertyWidget.depends = Core
