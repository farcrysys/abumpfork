/* A Bison parser, made by GNU Bison 3.0.5.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_HOME_LWH_L_SRC_ABUMP_CLONEBUILD_BF_SOURCES_UTILS_QTNPROPERTY_PEG_PROPERTYENUM_PARSER_HPP_INCLUDED
# define YY_YY_HOME_LWH_L_SRC_ABUMP_CLONEBUILD_BF_SOURCES_UTILS_QTNPROPERTY_PEG_PROPERTYENUM_PARSER_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    PROPERTY_SET = 258,
    PROPERTY = 259,
    INCLUDE_H = 260,
    INCLUDE_CPP = 261,
    INCLUDE_NAME = 262,
    EXTERN = 263,
    ID = 264,
    CPP_BRACKET_OPEN = 265,
    CPP_BRACKET_CLOSE = 266,
    ASSIGN = 267,
    SEMICOLON = 268,
    COLON = 269,
    DBL_COLON = 270,
    COMMA = 271,
    DOT = 272,
    TILDE = 273,
    VALUE_TO_ASSIGN = 274,
    ACCESS_ID = 275,
    SLOT = 276,
    DELEGATE = 277,
    CODE_H = 278,
    CODE_CPP = 279,
    CPP_CODE = 280,
    ENUM = 281,
    NUMBER = 282,
    STR = 283,
    INITIALIZATION_LIST = 284
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_HOME_LWH_L_SRC_ABUMP_CLONEBUILD_BF_SOURCES_UTILS_QTNPROPERTY_PEG_PROPERTYENUM_PARSER_HPP_INCLUDED  */
