This is a clone of a fork of the original QtnProperty intended for use with AwesomeBump
The version this one is cloned from is https://github.com/kmkolasinski/QtnProperty/
The original version is here: https://github.com/lexxmark/QtnProperty

**To build alone:**
  
    mkdir path_to_build
    cd path_to_build
    qmake ../Property.pro -r
    make

Generated libraries and executables will be placed into the 'path\_to\_build/bin' folder.
  
**To run tests and demo:**

    cd path_to_build/bin
    ./QtnPropertyTests

