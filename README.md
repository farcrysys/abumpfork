This is a fork of AwesomeBump intended to try to fix build issues

Please note build/run/install directions from the original may not
apply to this version

The original program is here https://github.com/kmkolasinski/AwesomeBump

AwesomeBump 5.0 (2016)
====================

AwesomeBump is a free and open source program written using Qt library designed to generate normal, height, specular or ambient occlusion, metallic, roughness textures from a single image. Additional features like material textures or grunge maps are available. Since the image processing is done in 99% on GPU the program runs very fast and all the parameters can be changed in real time. AB was made to be a new alternative to known gimp plugin called Insane Bump or commercial tool: Crazy Bump.

**AwesomeBump** is totaly written in Qt thus you don’t need to install any aditionall libraries. Just download and install Qt SDK, download the project from the repository, build and run. It will work (or should) on any platform supported by Qt.

#### What can AwesomeBump do?

* convert from normal map to height map,
* convert from height map (bump map) to normal map,
* extract the bump from the arbitrary image,
* calculate ambient occlusion and specularity of image.
* perspective tranformation of the image,
* creating seamless texture (simple linear filter, random mode, or mirror filter),
* generate roughness and metallic textures (different types of surface analysis are available),
* real time tessellation is available,
* saving images to following formats: PNG, JPG,BMP,TGA
* edit one texture which contains different materials
* add some grunge to your map with grunge texture.
* mix two bumpmaps togheter with normal map mixer.
* and many others, see our videos on YouTube

#### Contributors:
I would like to thanks those people for thier big effort to make this software more awesome!

* [ppiecuch](https://github.com/ppiecuch)
* [hevedy](https://github.com/Hevedy)
* [robert42](https://github.com/Robert42)
* [CodePhase](https://github.com/CodePhase)
* [Calinou](https://github.com/Calinou)
* and many others !!!


Building on Linux
-----------------
0. Install pre-requisites (on top of regular build tools and compiler):
	Debian: qt5-default qtscript5-dev libqt5script5
	Fedora: qt5-devel
1. Go into the source folder
3. Edit **unixBuildScript.sh** file and set the Qt5 path to qmake/qmake-qt5/etc
4. Run the build script: `sh unixBuildScript.sh`
5. If it succeeds there should be a Bin/abump
6. To run it cd Bin and run ./abump

### OpenGL 3.30 support

You can now build AB to run all openGL instructions with 3.30 compatibility (note that tessellation will not work with 3.30). See [PDF ](https://github.com/kmkolasinski/AwesomeBump/releases/download/BuildingAB/BuildingInstruction.pdf) file and "Step 8" for more datails. Basically you just have to add `CONFIG+=gl330` command in the qmake settings in order to build 3.30-supported version of AwesomeBump.

License
=======

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program (see LICENSE.txt). If not, see <https://www.gnu.org/licenses/>.

## third party licenses:
 * Using tinyobjloader for loading OBJ files: https://github.com/syoyo/tinyobjloader BSD license
 * Cube map textures were taken from Humus page [link](http://www.humus.name/index.php?page=Textures) CCBy 3.0 license
 * Most of the GUI controls are done with the [QtnProperty](https://github.com/lexxmark/QtnProperty) framework. Apache 2.0 license
 * Some QT examples are BSD license
 * Some QT generated files are LGPL 2.1 or 3
 * The Sources/gpuinfo.\* files are GPL 3
 * The rest are LGPL version 3 or later

