#!/bin/bash
# Set this to the location of qt5 qmake
MY_QMAKE_PATH=qmake
MY_WORKDIR=$PWD

wget="wget"
tool="gcc_64"
exe=""

if [[ "$OSTYPE" == "darwin"* ]]; then
	wget="curl -L -o master.zip"
	tool="clang_64"
	exe=".app"
fi

if [ ! -e "$MY_QMAKE_PATH" ]; then
	echo " Missing '$MY_QMAKE_PATH'."	
	echo " Set the MY_QMAKE_PATH variable in unixBuildScript.sh"
	echo ""
	exit 1
fi

# Have to build QtnProperty first
rm -rf Sources/utils/QtnProperty/build && \
	mkdir Sources/utils/QtnProperty/build && \
	cd Sources/utils/QtnProperty/build && \
	$MY_QMAKE_PATH ../Property.pro -r && \
	make

cd $MY_WORKDIR && \
	$MY_QMAKE_PATH && \
	make && \
	echo "*** Copying binary from `cat workdir/current` ..." && \
	cp -vr workdir/`cat workdir/current`/bin/abumpfork$exe ./Bin
